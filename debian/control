Source: coyim
Section: net
Priority: optional
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Sascha Steinbiss <satta@debian.org>
Build-Depends: debhelper (>= 11),
               dh-golang,
               golang-go (>= 2:1.7),
               golang-dbus-dev,
               golang-github-gotk3-gotk3-dev,
               golang-github-hydrogen18-stalecucumber-dev,
               golang-github-thecreeper-go-notify-dev,
               golang-github-twstrike-gotk3adapter-dev (>= 0.0~git20170505.0.901a95d),
               golang-github-twstrike-otr3-dev,
               golang-github-dhowett-go-plist-dev,
               golang-golang-x-crypto-dev,
               golang-gopkg-check.v1-dev,
               golang-github-miekg-dns-dev,
               golang-github-kardianos-osext-dev,
               pkg-config,
               asciidoctor
Standards-Version: 4.2.1
Homepage: https://coy.im
Vcs-Browser: https://salsa.debian.org/pkg-go-team/packages/coyim
Vcs-Git: https://salsa.debian.org/pkg-go-team/packages/coyim.git
XS-Go-Import-Path: github.com/twstrike/coyim

Package: coyim
Architecture: any
Built-Using: ${misc:Built-Using}
Depends: ${shlibs:Depends},
         ${misc:Depends}
Recommends: tor
Description: safe and secure XMPP chat client
 CoyIM is a chat client using the XMPP protocol. It is built upon
 https://github.com/agl/xmpp-client and https://github.com/twstrike/otr3.
 It adds a graphical user interface and tries to be safe and secure by default.
 The authors' ambition is that it should be possible for even the most
 high-risk people on the planet to safely use CoyIM, without having to make
 any configuration changes.
 .
 To do this, OTR is enabled by default, Tor is used by default, and the Tor
 Onion Service will be used for a server if it is known. Also, TLS and TLS
 certificates are used to verify the connection - no configuration necessary.
 The implementation is written in the Go language, to avoid many common types
 of vulnerabilities that come from using unsafe languages.
